/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class PropertiesFile {
	
	@Value("${conciliador.cron}")
	private String conciliadorCron;
	
	@Value("${stp.service.url}")
	private String stpServiceUrl;
	
	@Value("${stp.service.username}")
	private String stpServiceUsername;
	
	@Value("${stp.service.password}")
	private String stpServicePassword;
	
}
