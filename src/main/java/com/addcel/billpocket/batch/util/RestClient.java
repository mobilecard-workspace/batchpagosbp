/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.util;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.addcel.billpocket.batch.stp.response.BaseResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestClient {
	
	private static final Logger LOGGER = LogManager.getLogger(RestClient.class);
	
	@Autowired
	private PropertiesFile propsFile;

	public HttpHeaders addHeaders() {
		String basicAuth = propsFile.getStpServiceUsername() + ":" + propsFile.getStpServicePassword();
		basicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Basic " + basicAuth);
		
		return headers;
	}
	
	public BaseResponse createRequest(Object requestBody) 
			throws Exception {
		
		BaseResponse response = null;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		ObjectMapper mapper = new ObjectMapper();
		String uri = propsFile.getStpServiceUrl();
		String body = mapper.writeValueAsString(requestBody);
		
		HttpEntity<String> entity = new HttpEntity<String>(body, addHeaders());
				
		LOGGER.info("Enviando peticion a la url: " + uri);
		LOGGER.debug("Body Request: " + entity.getBody());
		
		ResponseEntity<BaseResponse> result = restTemplate.exchange(uri, HttpMethod.POST, entity, BaseResponse.class);
		response = result.getBody();
		
		LOGGER.debug("Status Code: " + result.getStatusCode());
		LOGGER.debug("Body Response: " + result.getBody());
		
		return response;
	}
	
}
