/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import({ SchedulerConfig.class })
@SpringBootApplication
public class PagosBatchBPApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagosBatchBPApplication.class, args);
	}
	
}
