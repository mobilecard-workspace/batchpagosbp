/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.billpocket.batch.entities.TBitacoras;

public interface TBitacorasRepository extends CrudRepository<TBitacoras, Long> {
	
	@Query(value = "SELECT sum(bit_cargo) FROM t_bitacora where id_aplicacion = :idAplicacion and "
			+ "id_producto = :idProducto and bit_hora >= :fechaIni and bit_hora <= :fechaFin", nativeQuery = true)
	public Double sumCargoByIdAplicacionAndIdProductoAndHoraBetween(@Param("idAplicacion") int idAplicacion, 
			@Param("idProducto") int idProducto, @Param("fechaIni") String fechaIni, @Param("fechaFin") String fechaFin);
	
}
