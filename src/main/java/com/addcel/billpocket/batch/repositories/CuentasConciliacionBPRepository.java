/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.addcel.billpocket.batch.entities.CuentasConciliacionBP;

public interface CuentasConciliacionBPRepository extends CrudRepository<CuentasConciliacionBP, Integer> {

	public List<CuentasConciliacionBP> findByActiva(int activa);
	
}
