/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.billpocket.batch.entities.BatchConciliacionBP;

public interface BatchConciliacionBPRepository extends CrudRepository<BatchConciliacionBP, Long> {
	
	@SuppressWarnings("unchecked")
	public BatchConciliacionBP save(BatchConciliacionBP batchConciliacionBP);
	
	@Query(value = "select * from batch_conciliacion_bp where stp_transfer_exitosa = :stpTransferExitosa and "
			+ "fecha >= :fechaIni and fecha <= :fechaFin", nativeQuery = true)
	public List<BatchConciliacionBP> findByFechaAndStpTransferExitosa(@Param("stpTransferExitosa") int stpTransferExitosa, 
			@Param("fechaIni") String fechaIni, @Param("fechaFin") String fechaFin);
	
}
