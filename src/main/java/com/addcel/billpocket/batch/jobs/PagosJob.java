/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.addcel.billpocket.batch.services.PagosService;

public class PagosJob implements Job {

	@Autowired
    private PagosService pagosServ;
 
    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
    	pagosServ.processData();
    }
	
}
