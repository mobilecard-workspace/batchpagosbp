/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch;

import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.addcel.billpocket.batch.jobs.PagosJob;
import com.addcel.billpocket.batch.quartz.AutowiringSpringBeanJobFactory;

public class SchedulerConfig {

	private static final Logger LOGGER = LogManager.getLogger(SchedulerConfig.class);
	
    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext) {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        
        return jobFactory;
    }
 
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(JobFactory jobFactory, Trigger cronJobTrigger)
            throws IOException {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setJobFactory(jobFactory);
        factory.setQuartzProperties(quartzProperties());
        factory.setTriggers(cronJobTrigger);
        
        LOGGER.debug("Starting jobs ....");
        
        return factory;
    }
    
    @Bean
    public static CronTriggerFactoryBean createCronTrigger(JobDetail jobDetail, 
    		@Value("${conciliador.cron}")String cronExpression) {
    	CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
    	factoryBean.setJobDetail(jobDetail);
    	factoryBean.setCronExpression(cronExpression);
    	factoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
    	
    	return factoryBean;
    }
    
    @Bean
    public Properties quartzProperties() throws IOException {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
        propertiesFactoryBean.afterPropertiesSet();
        
        return propertiesFactoryBean.getObject();
    }
 
    @Bean
    public static JobDetailFactoryBean simpleJobDetail() {
        JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
        factoryBean.setJobClass(PagosJob.class);
        factoryBean.setDurability(true);
        
        return factoryBean;
    }
	
}
