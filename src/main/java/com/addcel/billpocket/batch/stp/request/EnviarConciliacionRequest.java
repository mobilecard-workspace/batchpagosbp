/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.stp.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnviarConciliacionRequest {

	private String conceptoPago;
	private String cuentaBeneficiario;
	private String institucionContraparte;
	private Double monto;
	private String nombreBeneficiario;
	private String tipoCuentaBeneficiario;
	
}
