/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "batch_conciliacion_bp")
public class BatchConciliacionBP {
	
	@Id
	@Column(name = "id_batch_conciliacion_bp", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idBatchConciliacionBp;
	
	@Column(name = "fecha", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
		
	@Column(name = "id_aplicacion", nullable = false)
	private int idAplicacion;
	
	@Column(name = "id_producto", nullable = false)
	private int idProducto;
		
	@Column(name = "monto_total", nullable = true)
	private BigDecimal montoTotal;
	
	@Column(name = "id_cuentas_conciliacion_bp", nullable = false)
	private int idCuentasConciliacionBp;
	
	@Column(name = "stp_transfer_exitosa", nullable = false)
	private int stpTransferExitosa;
	
	@Column(name = "stp_error", nullable = true, length = 200)
	private String stpError;
	
	@Column(name = "stp_id_orden", nullable = true, length = 45)
	private String stpIdOrden;
	
	@Column(name = "stp_folio", nullable = true, length = 50)
	private String stpFolio;
	
	@Column(name = "stp_clave_rastreo", nullable = true, length = 30)
	private String stpClaveRastreo;
	
	@Column(name = "stp_referencia", nullable = true, length = 7)
	private String stpReferencia;

}
