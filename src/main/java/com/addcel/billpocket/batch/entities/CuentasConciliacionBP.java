/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cuentas_conciliacion_bp")
public class CuentasConciliacionBP {
	
	@Id
	@Column(name = "id_cuentas_conciliacion_bp", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCuentasConciliacionBp;
			
	@Column(name = "id_aplicacion", nullable = false)
	private int idAplicacion;
	
	@Column(name = "id_producto", nullable = false)
	private int idProducto;
	
	@Column(name = "clabe", nullable = false, length = 45)
	private String clabe;
	
	@Column(name = "stp_tipo_cuenta", nullable = false)
	private int stpTipoCuenta;
	
	@Column(name = "stp_id_institucion_bancaria", nullable = false)
	private int stpIdInstitucionBancaria;
	
	@Column(name = "nombre_beneficiario", nullable = false, length = 45)
	private String nombreBeneficiario;
	
	@Column(name = "concepto_pago", nullable = false, length = 45)
	private String conceptoPago;
	
	@Column(name = "activa", nullable = false)
	private int activa;

}
