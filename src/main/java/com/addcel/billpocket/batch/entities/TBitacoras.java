/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_bitacoras")
public class TBitacoras {

	@Id
	@Column(name = "id_bitacora", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idBitacora;
	
	@Column(name = "id_usuario", nullable = false)
	private long idUsuario;
	
	@Column(name = "id_aplicacion", nullable = false)
	private int idAplicacion;
	
	@Column(name = "id_proveedor", nullable = false)
	private int idProveedor;
	
	@Column(name = "id_producto", nullable = false)
	private int idProducto;
	
	@Column(name = "bit_hora", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date bitHora;
	
	@Column(name = "bit_fecha", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date bitFecha;
	
	@Column(name = "bit_cargo", nullable = true)
	private BigDecimal bitCargo;
	
	@Column(name = "bit_status", nullable = true)
	private int bitStatus;
	
}
