/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.constants;

public class BatchBPConstants {

	public static final int CUENTA_ACTIVA = 1;
	public static final int CUENTA_INACTIVA = 0;
	
	public static final int TRANSFER_EXITOSA = 1;
	public static final int TRANSFER_FALLIDA = 0;
	
	public static final String HOUR_INI = " 00:00:00";
	public static final String HOUR_FIN = " 23:59:59";
	
	public static final int STP_SUCCESS_CODE = 1000;
	
	public static final String STP_CONSUMER_ERROR  = "No se pudo conectar con el servicio de STP Consumer";
	public static final String NO_TRANSACTIONS = "No se encontraron transacciones para esa cuenta";
	
}
