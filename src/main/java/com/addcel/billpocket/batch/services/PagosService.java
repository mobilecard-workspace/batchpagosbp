/**
 * @author Victor Ramirez
 */

package com.addcel.billpocket.batch.services;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.billpocket.batch.constants.BatchBPConstants;
import com.addcel.billpocket.batch.entities.BatchConciliacionBP;
import com.addcel.billpocket.batch.entities.CuentasConciliacionBP;
import com.addcel.billpocket.batch.repositories.BatchConciliacionBPRepository;
import com.addcel.billpocket.batch.repositories.CuentasConciliacionBPRepository;
import com.addcel.billpocket.batch.repositories.TBitacorasRepository;
import com.addcel.billpocket.batch.stp.request.EnviarConciliacionRequest;
import com.addcel.billpocket.batch.stp.response.BaseResponse;
import com.addcel.billpocket.batch.util.RestClient;

@Service
public class PagosService {
	
	private static final Logger LOGGER = LogManager.getLogger(PagosService.class);
	
	@Autowired
	private CuentasConciliacionBPRepository cuentasConciliaBPRepo;
	
	@Autowired
	private TBitacorasRepository tBitacorasRepo;
	
	@Autowired
	private BatchConciliacionBPRepository batchConciliacionBpRepo;
	
	@Autowired
	private RestClient restClient;

	public void processData() {
		GregorianCalendar calendar = new GregorianCalendar();
		
		String fechaHoyIni = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1 <= 9 ? "0" : "") + (calendar.get(Calendar.MONTH) + 1) +
				"-" + calendar.get(Calendar.DAY_OF_MONTH) + BatchBPConstants.HOUR_INI;
		String fechaHoyFin = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1 <= 9 ? "0" : "") + (calendar.get(Calendar.MONTH) + 1) +
				"-" + calendar.get(Calendar.DAY_OF_MONTH) + BatchBPConstants.HOUR_FIN;
		
		calendar.add(Calendar.DATE, -1);
		
		String fechaAyerIni = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1 <= 9 ? "0" : "") + (calendar.get(Calendar.MONTH) + 1) +
				"-" + calendar.get(Calendar.DAY_OF_MONTH) + BatchBPConstants.HOUR_INI;
		String fechaAyerFin = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1 <= 9 ? "0" : "") + (calendar.get(Calendar.MONTH) + 1) +
				"-" + calendar.get(Calendar.DAY_OF_MONTH) + BatchBPConstants.HOUR_FIN;
		
		LOGGER.info("Buscando si hay conciliaciones pendientes por transferir para hoy");
		List<BatchConciliacionBP> batchConciliaPendientesList = batchConciliacionBpRepo.
				findByFechaAndStpTransferExitosa(BatchBPConstants.TRANSFER_FALLIDA, fechaHoyIni, fechaHoyFin);
		
		List<BatchConciliacionBP> batchConciliaExitosasList = batchConciliacionBpRepo.
				findByFechaAndStpTransferExitosa(BatchBPConstants.TRANSFER_EXITOSA, fechaHoyIni, fechaHoyFin);
		
		if(!batchConciliaPendientesList.isEmpty()) {
			LOGGER.info("Cantidad de conciliaciones pendientes por enviar hoy: " + batchConciliaPendientesList.size());
			for(BatchConciliacionBP batchConciliaPendiente : batchConciliaPendientesList) {
				LOGGER.debug("Buscando la cuenta en donde se enviara la conciliacion pendiente ...");
				CuentasConciliacionBP cuentaConciliaBP = cuentasConciliaBPRepo.findOne(batchConciliaPendiente.getIdCuentasConciliacionBp());
				LOGGER.debug("Cuenta a la que se enviara la conciliacion pendiente: " + cuentaConciliaBP.getNombreBeneficiario());
				
				batchConciliaPendiente.setFecha(new Date());
				
				LOGGER.info("Monto total pendiente a transferir para esa cuenta: " + batchConciliaPendiente.getMontoTotal().doubleValue());
				enviarDinero(cuentaConciliaBP, batchConciliaPendiente.getMontoTotal().doubleValue(), batchConciliaPendiente, true);
				
				LOGGER.debug("Actualizando la conciliacion pendiente en la BD ...");
				BatchConciliacionBP batchConciliacionSaved = batchConciliacionBpRepo.save(batchConciliaPendiente);
				LOGGER.info("Conciliacion pendiente actualizada en la BD: " + batchConciliacionSaved.toString());
			}
		} else if(batchConciliaExitosasList.isEmpty()) {
			LOGGER.info("No hay conciliaciones pendientes para hoy");
			
			LOGGER.info("Iniciando conciliacion de transacciones con fecha desde: " + fechaAyerIni);
			LOGGER.info("Hasta la fecha: " + fechaAyerFin);
			
			LOGGER.debug("Buscando las cuentas para enviar el dinero de las conciliaciones");
			List<CuentasConciliacionBP> cuentasConciliacionList = cuentasConciliaBPRepo.findByActiva(BatchBPConstants.CUENTA_ACTIVA);
			
			if(cuentasConciliacionList != null && !cuentasConciliacionList.isEmpty()) {
				for(CuentasConciliacionBP cuentasConciliacion : cuentasConciliacionList) {
					LOGGER.info("Iniciando la conciliacion para la cuenta: " + cuentasConciliacion.getNombreBeneficiario());
					LOGGER.debug("Detalle de la cuenta de conciliacion: " + cuentasConciliacion.toString());
					
					int idAplicacion = cuentasConciliacion.getIdAplicacion();
					int idProducto = cuentasConciliacion.getIdProducto();
					
					Double monto = tBitacorasRepo.sumCargoByIdAplicacionAndIdProductoAndHoraBetween(idAplicacion, idProducto, fechaAyerIni, fechaAyerFin);
					
					BatchConciliacionBP batchConciliacion = new BatchConciliacionBP();
					batchConciliacion.setIdAplicacion(idAplicacion);
					batchConciliacion.setIdCuentasConciliacionBp(cuentasConciliacion.getIdCuentasConciliacionBp());
					batchConciliacion.setIdProducto(idProducto);
					batchConciliacion.setFecha(new Date());
					
					if(monto != null) {
						LOGGER.info("Monto total a transferir para esa cuenta: " + monto);
						batchConciliacion.setMontoTotal(BigDecimal.valueOf(monto));
												
						enviarDinero(cuentasConciliacion, monto, batchConciliacion, false);
					} else {
						LOGGER.warn("No se encontraron transacciones realizadas durante el dia para esa cuenta");
						batchConciliacion.setStpTransferExitosa(BatchBPConstants.TRANSFER_FALLIDA);
						batchConciliacion.setStpError(BatchBPConstants.NO_TRANSACTIONS);
					}
					
					LOGGER.debug("Guardando conciliacion en la BD ...");
					BatchConciliacionBP batchConciliacionSaved = batchConciliacionBpRepo.save(batchConciliacion);
					LOGGER.info("Bitacora de conciliacion guardada en la BD: " + batchConciliacionSaved.toString());
				}
			} else {
				LOGGER.warn("No se encontraron cuentas para enviar el dinero de la conciliacion");
			}
		} else {
			LOGGER.info("Se ha realizado todas las conciliaciones correctamente, no hay envios de dinero pendientes");
		}
	}
	
	/**
	 * 
	 * @param cuentasConciliacion la cuenta a la que se va a transferir el dinero
	 * @param monto la cantidad a transferir
	 * @param batchConciliacion el regisro en BD que se guardara para esa conciliacion
	 * @param pendiente bandera para saber si se va a enviar el dinero de una conciliacion pendiente, 
	 * true si es pendiente y false si es una nueva
	 */
	public void enviarDinero(CuentasConciliacionBP cuentasConciliacion, Double monto, BatchConciliacionBP batchConciliacion, boolean pendiente) {
		if(pendiente) LOGGER.info("Volviendo a intentar transferir el dinero de la conciliacion pendiente ...");
		else LOGGER.info("Transfiriendo el dinero de la conciliacion ...");
		
		EnviarConciliacionRequest enviarConciliaReq = new EnviarConciliacionRequest();
		enviarConciliaReq.setConceptoPago(cuentasConciliacion.getConceptoPago());
		enviarConciliaReq.setCuentaBeneficiario(cuentasConciliacion.getClabe());
		enviarConciliaReq.setInstitucionContraparte(String.valueOf(cuentasConciliacion.getStpIdInstitucionBancaria()));
		enviarConciliaReq.setMonto(monto);
		enviarConciliaReq.setNombreBeneficiario(cuentasConciliacion.getNombreBeneficiario());
		enviarConciliaReq.setTipoCuentaBeneficiario(String.valueOf(cuentasConciliacion.getStpTipoCuenta()));
				
		try {
			BaseResponse baseResp = restClient.createRequest(enviarConciliaReq);
			
			LOGGER.debug("STP Consumer Codigo de Respuesta: " + baseResp.getCode());
			LOGGER.debug("STP Consumer Mensaje de Respuesta: " + baseResp.getMessage());
			
			if(baseResp.getCode() == BatchBPConstants.STP_SUCCESS_CODE) {
				if(pendiente) LOGGER.info("Se ha enviado correctamente el dinero de la conciliacion que estaba pendiente");
				else LOGGER.info("Se ha enviado correctamente el dinero de la conciliacion");
				
				batchConciliacion.setStpTransferExitosa(BatchBPConstants.TRANSFER_EXITOSA);
				batchConciliacion.setStpClaveRastreo(baseResp.getClaveRastreo());
				batchConciliacion.setStpError(null);
				batchConciliacion.setStpFolio(baseResp.getFolio());
				batchConciliacion.setStpIdOrden(String.valueOf(baseResp.getIdOperacion()));
				batchConciliacion.setStpReferencia(baseResp.getReferencia());
			} else {
				if(pendiente) LOGGER.warn("No se pudo enviar el dinero de la conciliacion que estaba pendiente: " + baseResp.getMessage());
				else LOGGER.warn("No se pudo enviar el dinero de la conciliacion: " + baseResp.getMessage());
				
				batchConciliacion.setStpTransferExitosa(BatchBPConstants.TRANSFER_FALLIDA);
				batchConciliacion.setStpError(baseResp.getCode() + " - " + baseResp.getMessage());
			}
		} catch (Exception ex) {
			if(pendiente) LOGGER.error("No se pudo volver a realizar la conciliacion que estaba pendiente debido a que hubo un error del servicio de STP Consumer: " + ex.getMessage());
			else LOGGER.error("Ocurrio un error con el servicio de STP Consumer al enviar el dinero de la conciliacion: " + ex.getMessage());
			
			ex.printStackTrace();
			batchConciliacion.setStpTransferExitosa(BatchBPConstants.TRANSFER_FALLIDA);
			batchConciliacion.setStpError(BatchBPConstants.STP_CONSUMER_ERROR);
		}
	}
}
